import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class AuthServiceAcceptanceIT {


    private AuthServiceClient client;

    @BeforeEach
    void setUp() {
        client = new AuthServiceClient();
    }

    @Test
    public void testScenario() {

        final String userId1 = createAndAssertUser("1someemail", "somepassword", "somefname", "somesname", true);
        final String userId2 = createAndAssertUser("2someemail", "somepassword", "somefname", "somesname", true);
        final String userId3 = createAndAssertUser("3someemail", "somepassword", "somefname", "somesname", true);

        assertUsersExist(userId1, userId2, userId3);

        deleteUser(userId1);
        deleteUser(userId2);

        assertNotFound(userId1);
        assertNotFound(userId2);
        assertFound(userId3);
    }

    private String createAndAssertUser(String email, String password, String firstName, String surname, boolean isActive) {

        final User user = new User().setIsActive(isActive).setEmail(email).setFirstName(firstName).setPassword(password).setSurname(surname);

        final String id = client.create(user);
        final User loadedUser = client.retrieve(id);

        assertThat(loadedUser).isEqualToComparingOnlyGivenFields(user, "email", "password", "firstName", "surname", "isActive");

        return id;
    }

    private void assertUsersExist(String... userIds) {

        final List<String> loadedUserIds = client.retrieve();
        assertThat(loadedUserIds).contains(userIds);
    }

    private void deleteUser(final String id) {
        client.delete(id);
    }

    private void assertNotFound(final String id) {
        assertThat(client.retrieve(id)).isNull();
    }

    private void assertFound(final String id) {
        assertThat(client.retrieve(id)).isNotNull();
    }
}
