import javax.json.bind.JsonbBuilder;

public class User {

    private long id;
    private String email;
    private String password;
    private String firstName;
    private String surname;
    private boolean isActive;

    public long getId() {
        return id;
    }

    public User setId(long id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public User setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public User setIsActive(boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}