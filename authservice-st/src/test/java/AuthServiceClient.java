
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import static org.assertj.core.api.Assertions.assertThat;

public class AuthServiceClient {

    private Client client;
    private WebTarget baseTarget;
    private String usersUri = "http://localhost:4444/authservice/resources/users";

    public AuthServiceClient() {
        client = ClientBuilder.newClient();
        baseTarget = client.target(URI.create(usersUri));
    }

    public User retrieve(String id) {

        final Response response = baseTarget.path(id).request(MediaType.APPLICATION_JSON_TYPE).get();

        if (response.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            return null;
        }

        assertStatus(response, Response.Status.OK);
        return JsonbBuilder.create().fromJson(response.readEntity(String.class), User.class);
    }

    public List<String> retrieve() {

        final Response response = baseTarget.request(MediaType.APPLICATION_JSON_TYPE).get();
        assertStatus(response, Response.Status.OK);
        return extractUserIds(response);
    }

    private List<String> extractUserIds(final Response response) {
        List<User> users = JsonbBuilder.create().fromJson(response.readEntity(String.class), new ArrayList<User>(){}.getClass().getGenericSuperclass());

        return users.stream()
                .map(o -> o.getId())
                .map(String::valueOf)
                .collect(Collectors.toList());
    }

    public String create(User user) {

        final Response response = baseTarget.request().post(Entity.json(user.toJson()));
        assertStatus(response, Response.Status.CREATED);

        long id = JsonbBuilder.create().fromJson(response.readEntity(String.class), User.class).getId();
        assertThat(id).isNotNull();
        return String.valueOf(id);
    }

    private void assertStatus(final Response response, final Response.Status status) {
        assertThat(response.getStatus()).isEqualTo(status.getStatusCode());
    }

    public void delete(String id) {
        final Response response = baseTarget.path(id).request(MediaType.APPLICATION_JSON_TYPE).delete();
        assertStatus(response, Response.Status.NO_CONTENT);
    }
}