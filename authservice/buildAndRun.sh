#!/bin/sh
mvn clean package && docker build -t com.nttdatalab/authservice .
docker rm -f authservice || true && docker run -d -p 8081:8080 -p 4848:4848 -p 9009:9009 --network authdb-network --name authservice com.nttdatalab/authservice 
