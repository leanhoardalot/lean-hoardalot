#!/bin/sh
docker build -t com.nttdatalab/authdb .
docker rm -f authdb || true && docker run -d --network authdb-network --name authdb com.nttdatalab/authdb
