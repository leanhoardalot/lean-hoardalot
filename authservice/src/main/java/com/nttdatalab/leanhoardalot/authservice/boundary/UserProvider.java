package com.nttdatalab.leanhoardalot.authservice.boundary;


import com.nttdatalab.leanhoardalot.authservice.entity.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class UserProvider {

    @PersistenceContext
    EntityManager em;

    public void createUser(User user) {
        em.persist(user);
    }

    public User getUser(long id) {
        return em.find(User.class, id);
    }

    public List<User> getUsers() {
        return em.createNamedQuery(User.FIND_ALL, User.class).getResultList();
    }

    public void updateUser(long id, User user) {
        User managedUser = em.find(User.class, id);
        managedUser.setEmail(user.getEmail());
        managedUser.setFirstName(user.getFirstName());
        managedUser.setIsActive(user.getIsActive());
        managedUser.setPassword(user.getPassword());
        managedUser.setSurname(user.getSurname());
    }

    public void deleteUser(long id) {
        User managedUser = em.find(User.class, id);
        em.remove(managedUser);
    }
}