package com.nttdatalab.leanhoardalot.authservice.boundary;

import com.nttdatalab.leanhoardalot.authservice.entity.User;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("users")
public class UserResource {

    @Inject
    UserProvider userProvider;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(@NotNull final User user) {
        userProvider.createUser(user);
        return Response.status(Response.Status.CREATED).entity(user).build();
    }

    @GET
    public List<User> getUsers() {
        return userProvider.getUsers();
    }

    @GET
    @Path("{id}")
    public Response getUser(@PathParam("id") long id) {
        final User user = userProvider.getUser(id);

        if (user == null)
            return Response.status(Response.Status.NOT_FOUND).entity("User '"+ id + "' not found").build();

        return Response.ok(user).build();
    }

    @PUT
    @Path("{id}")
    public void updateUser(@PathParam("id") long id, User user) {
        userProvider.updateUser(id, user);
    }

    @DELETE
    @Path("{id}")
    public void deleteUser(@PathParam("id") long id) {
        userProvider.deleteUser(id);
    }

}
