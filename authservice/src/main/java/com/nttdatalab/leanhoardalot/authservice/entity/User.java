package com.nttdatalab.leanhoardalot.authservice.entity;


import javax.json.bind.JsonbBuilder;
import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.*;
import java.util.Objects;

import static com.nttdatalab.leanhoardalot.authservice.entity.User.FIND_ALL;


@Entity
@Table(name = "USERS")
@NamedQueries({
        @NamedQuery(name = FIND_ALL, query = "select u from User u")})
public class User {

    public static final String FIND_ALL = "User.findAll";

    @Id
    @GeneratedValue
    private long id;

    @Basic(optional = false)
    private String email;

    @Basic(optional = false)
    private String password;

    @Basic(optional = false)
    private String firstName;

    @Basic(optional = false)
    private String surname;

    @Basic(optional = false)
    public boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}
